﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataAccess
{
    public sealed class Tool
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public List<Recipe> Recipes { get; set; }

        public static void MakeNameUnique(ModelBuilder builder)
        {
            builder.Entity<Tool>()
                .HasIndex(u => u.Name)
                .IsUnique();
        }
    }
}