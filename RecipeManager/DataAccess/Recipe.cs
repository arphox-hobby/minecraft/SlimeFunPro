﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataAccess
{
    public sealed class Recipe
    {
        public int Id { get; set; }

        [Required]
        public Item ResultItem { get; set; }
        public Tool RequiredTool { get; set; }
        public List<Ingredient> Ingredients { get; set; }
    }
}