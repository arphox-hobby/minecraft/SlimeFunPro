﻿using System.ComponentModel.DataAnnotations;

namespace DataAccess
{
    public sealed class Ingredient
    {
        public int Id { get; set; }

        [Required]
        public Recipe Recipe { get; set; }

        [Required]
        public Item Item { get; set; }

        [Required]
        public int Amount { get; set; }

        public int? Position { get; set; }
    }
}