﻿using Microsoft.EntityFrameworkCore;

namespace DataAccess
{
    public sealed class RecipeStoreContext : DbContext
    {
        public DbSet<Item> Items { get; set; }
        public DbSet<Tool> Tools { get; set; }
        public DbSet<Recipe> Recipes { get; set; }
        public DbSet<Ingredient> Ingredients { get; set; }
        public DbSet<Research> Researches { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseNpgsql(
                "Host=localhost;" +
                "Database=SlimeFun_RecipeManager;" +
                "Username=slimefun_recipemanager;" +
                "Password=recipemanager");

        protected override void OnModelCreating(ModelBuilder builder)
        {
            Tool.MakeNameUnique(builder);
        }
    }
}