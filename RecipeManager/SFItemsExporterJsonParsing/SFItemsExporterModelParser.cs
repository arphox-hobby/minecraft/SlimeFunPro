﻿using System.Text.Json;

namespace SFItemsExporterJsonParsing
{
    public sealed class SFItemsExporterModelParser
    {
        public SFItemsExporterModel Parse(string json)
        {
            JsonSerializerOptions options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };

            SlimeFunItem[] items = JsonSerializer.Deserialize<SlimeFunItem[]>(json, options);
            return new SFItemsExporterModel
            {
                Items = items
            };
        }
    }
}