﻿using DataAccess;
using SFItemsExporterJsonParsing;
using System;
using System.Collections.Generic;

namespace RecipeManager
{
    public sealed class Transformer : IDisposable
    {
        private readonly RecipeStoreContext db;
        private Dictionary<string, SlimeFunItem> slimefunItemNameLookup;
        private Dictionary<string, Tool> toolLookupByName = new Dictionary<string, Tool>();
        private Dictionary<string, Research> researchLookupByName = new Dictionary<string, Research>();
        private Dictionary<string, Item> itemLookupById = new Dictionary<string, Item>();
        private Dictionary<string, Item> itemLookupByDisplayName = new Dictionary<string, Item>();

        public Transformer()
        {
            db = new RecipeStoreContext();
        }

        public void PurgeDb()
        {
            ConsoleLogger.WriteLine("Purging db...");
            db.Ingredients.RemoveRange(db.Ingredients);
            db.Items.RemoveRange(db.Items);
            db.Recipes.RemoveRange(db.Recipes);
            db.Researches.RemoveRange(db.Researches);
            db.Tools.RemoveRange(db.Tools);
            db.SaveChanges();
            ConsoleLogger.WriteLine("Purge done!");
        }

        public void Transform(SFItemsExporterModel model)
        {
            if (slimefunItemNameLookup != null) throw new Exception("hey do not reuse this object");

            ConsoleLogger.WriteLine("Starting Transform...");
            ConsoleLogger.WriteLine("Creating slimefunItemLookup...");
            slimefunItemNameLookup = CreateSlimefunItemNameLookup(model);

            foreach (SlimeFunItem slimeFunItem in model.Items)
            {
                ConsoleLogger.WriteLine($"Processing {slimeFunItem.Id}...");
                Tool tool = GetOrAddTool(slimeFunItem);
                Research research = GetOrAddResearch(slimeFunItem);
                Item item = GetOrAddItem(slimeFunItem);
                item.DisplayName = slimeFunItem.Item.Name ?? slimeFunItem.Item.Material;
                itemLookupByDisplayName[item.DisplayName] = item;
                item.Category = slimeFunItem.Category;
                item.CategoryTier = slimeFunItem.CategoryTier;
                item.Electric = slimeFunItem.Electric;
                item.Radioactive = slimeFunItem.Radioactive;
                item.UsableInWorkbench = slimeFunItem.UsableInWorkbench;
                item.Research = research;

                List<Ingredient> ingredients = CreateIngredients(slimeFunItem.Recipe);
                Recipe recipe = new Recipe
                {
                    ResultItem = item,
                    RequiredTool = tool,
                    Ingredients = ingredients
                };
                db.Recipes.Add(recipe);
            }

            ConsoleLogger.WriteLine("Processing done, saving changes to db...");
            db.SaveChanges();
            ConsoleLogger.WriteLine("Done!");
        }

        private static Dictionary<string, SlimeFunItem> CreateSlimefunItemNameLookup(SFItemsExporterModel model)
        {
            Dictionary<string, SlimeFunItem> dict = new Dictionary<string, SlimeFunItem>();
            foreach (SlimeFunItem sfItem in model.Items)
            {
                string key = sfItem.Item.Name ?? sfItem.Item.Material;
                SlimeFunItem value = sfItem;
                bool canAdd = dict.TryAdd(key, value);
                if (!canAdd)
                    ConsoleLogger.WriteLine($"'{sfItem.Item.Name}' could not be added because another item with the same name is already added.");
            }

            return dict;
        }

        private Tool GetOrAddTool(SlimeFunItem slimeFunItem)
        {
            string toolName = slimeFunItem.RecipeType;
            if (toolLookupByName.TryGetValue(toolName, out Tool existing))
                return existing;

            Tool tool = new Tool { Name = toolName };
            db.Tools.Add(tool);
            toolLookupByName.Add(toolName, tool);
            return tool;
        }

        private Research GetOrAddResearch(SlimeFunItem slimeFunItem)
        {
            string researchName = slimeFunItem.Research;
            if (researchName == null)
                return null;

            if (researchLookupByName.TryGetValue(researchName, out Research existing))
                return existing;

            Research research = new Research
            {
                Name = researchName,
                Cost = slimeFunItem.ResearchCost
            };
            db.Researches.Add(research);
            researchLookupByName.Add(researchName, research);
            return research;
        }

        private Item GetOrAddItem(SlimeFunItem slimeFunItem)
        {
            if (itemLookupById.TryGetValue(slimeFunItem.Id, out Item existing))
                return existing;

            Item item = new Item
            {
                Id = slimeFunItem.Id,
                Material = slimeFunItem.Item.Material
            };

            AddItem(item);
            return item;
        }

        private List<Ingredient> CreateIngredients(ItemStack[] recipe)
        {
            List<Ingredient> ingredients = new List<Ingredient>(9);
            for (int i = 0; i < recipe.Length; i++)
            {
                if (recipe[i] == null)
                    continue;

                int position = i + 1;
                ingredients.Add(CreateIngredient(recipe[i], position));
            }
            return ingredients;
        }

        private Ingredient CreateIngredient(ItemStack itemStack, int position)
        {
            Item item = GetOrCreateItem(itemStack);

            return new Ingredient
            {
                Position = position,
                Amount = 1,
                Item = item
            };
        }

        private Item GetOrCreateItem(ItemStack itemStack)
        {
            if (itemStack.Name == null)
                return GetOrAddSimpleVanillaItem(itemStack.Material);

            return GetOrCreateSlimefunItem(itemStack);
        }

        private Item GetOrCreateSlimefunItem(ItemStack itemStack)
        {
            if (itemLookupByDisplayName.TryGetValue(itemStack.Name, out Item existing))
                return existing;

            string id = slimefunItemNameLookup.GetValueOrDefault(itemStack.Name)?.Id;
            // When can the id be null here?
            // For a not-SlimeFun item, most likely a vanilla item with a special name
            // e.g. "Dispenser (Facing up)"

            Item item = new Item
            {
                Id = id ?? itemStack.Name,
                Material = itemStack.Material,
                DisplayName = itemStack.Name
            };
            AddItem(item);
            return item;
        }

        private Item GetOrAddSimpleVanillaItem(string material)
        {
            if (itemLookupById.TryGetValue(material, out Item existing))
                return existing;

            Item item = new Item { Id = material, Material = material };
            AddItem(item);
            return item;
        }

        private void AddItem(Item item)
        {
            db.Items.Add(item);
            itemLookupById.Add(item.Id, item);
            if (item.DisplayName != null)
                itemLookupByDisplayName.Add(item.DisplayName, item);
        }

        public void Dispose() => db.Dispose();
    }
}