﻿using System;
using System.Globalization;

namespace RecipeManager
{
    public static class ConsoleLogger
    {
        public static void WriteLine(string message)
        {
            string timestamp = DateTime.Now.ToString("HH:mm:ss.fff", CultureInfo.InvariantCulture);
            string log = $"[{timestamp}] {message}";
            Console.WriteLine(log);
        }
    }
}